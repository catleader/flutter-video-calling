import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:webview_videocall/utils.dart';
import 'package:webview_videocall/videocall.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Permission.camera.request();
  await Permission.microphone.request();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WebCall',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'WebCall'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final ChromeSafariBrowser _safariBrowser = ChromeSafariBrowser();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Enter URL',
              style: Theme.of(context).textTheme.titleMedium?.copyWith(
                    fontWeight: FontWeight.w700,
                  ),
            ),
            const SizedBox(height: 8,),
            TextField(
              decoration: appFormInputDecoration(context,
                  prefixIcon: const Icon(Icons.search)),
              onSubmitted: (value) {
                if (value.isNotEmpty) {
                  if (Platform.isIOS) {
                    _safariBrowser.open(
                      url: Uri.parse(value),
                      options: ChromeSafariBrowserClassOptions(
                        ios: IOSSafariOptions(
                          dismissButtonStyle: IOSSafariDismissButtonStyle.CLOSE,
                          presentationStyle:
                              IOSUIModalPresentationStyle.OVER_FULL_SCREEN,
                        ),
                      ),
                    );
                  } else {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        fullscreenDialog: true,
                        builder: (context) => VideoCall(url: value),
                      ),
                    );
                  }
                }
              },
            )
          ],
        ),
      ),
    );
  }
}
