import 'package:flutter/material.dart';

const appBorderRadius = BorderRadius.all(
  Radius.circular(
    12,
  ),
);

const Color borderColor = Color(0x20000000);

InputDecoration appFormInputDecoration(BuildContext context, {
  String hint = '',
  bool enable = true,
  Widget? prefixIcon,
}) {
  return InputDecoration(
    prefixIcon: prefixIcon,
    counterText: '',
    focusColor: Theme
        .of(context)
        .primaryColor,
    filled: !enable,
    fillColor: enable ? Colors.white : const Color(0xFFD6D6D6),
    enabledBorder: const OutlineInputBorder(
      borderRadius: appBorderRadius,
      borderSide: BorderSide(color: borderColor, width: 1),
    ),
    disabledBorder: const OutlineInputBorder(
      borderRadius: appBorderRadius,
      borderSide: BorderSide(color: borderColor, width: 1),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: appBorderRadius,
      borderSide: BorderSide(color: Theme
          .of(context)
          .primaryColor, width: 2),
    ),
    errorBorder: const OutlineInputBorder(
      borderRadius: appBorderRadius,
      borderSide: BorderSide(color: Colors.red, width: 2),
    ),
    focusedErrorBorder: const OutlineInputBorder(
      borderRadius: appBorderRadius,
      borderSide: BorderSide(color: Colors.red, width: 2),
    ),
    contentPadding: const EdgeInsets.symmetric(vertical: 12, horizontal: 12),
    hintText: hint,
    hintStyle: const TextStyle(
      color: Colors.black38,
    ),
  );
}